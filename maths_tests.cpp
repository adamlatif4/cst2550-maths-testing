#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "maths.h"

TEST_CASE("test primes", "[is_prime]")
{
  REQUIRE(is_prime(2) == true);
  REQUIRE(is_prime(3) == true);
  REQUIRE(is_prime(4) == false);
  REQUIRE(is_prime(5) == true);
  REQUIRE(is_prime(6) == false);
  REQUIRE(is_prime(7) == true);
  REQUIRE(is_prime(8) == false);
  REQUIRE(is_prime(9) == false);
}

TEST_CASE("test absolutes", "[absolute]")
{
  REQUIRE(absolute(-1) == 1);
  REQUIRE(absolute(-2) == 2);
  REQUIRE(absolute(-13) == 13);
  REQUIRE(absolute(3) == 3);
}

TEST_CASE("test powers", "[power]")
{
  REQUIRE(power(1, 5) == 1);
  REQUIRE(power(3, 3) == 27);
  REQUIRE(power(10, 10) == 10000000000);
  REQUIRE(power(2, 7) == 128);
}
